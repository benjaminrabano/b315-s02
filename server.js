const express = require("express");
const app = express();
const { Server } = require("socket.io");
const cors = require("cors");
const http = require("http");
const server = http.createServer(app);

const io = new Server(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
    credentials: true, // allows cookies, if needed
  },
});

const port = 3001;

// middleware
app.use(cors());

io.on("connection", (socket) => {
  console.log("A user is connected.");

  socket.on("chat_message", (message) => {
    const messageWithID = {
      id: socket.id,
      message,
    };
    io.emit("received_message", messageWithID);
  });

  socket.on("disconnect", () => {
    console.log("A user disconnected.");
  });
});

server.listen(port, () => {
  console.log(`App listening on port ${port}`);
});